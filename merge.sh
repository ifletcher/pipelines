#!/usr/bin/env bash

# Exit immediately if a any command exits with a non-zero status testing
# e.g. pull-request merge fails because of conflict
set -e

# Set destination branch
DEST_BRANCH=$1

# Set automatic merge
MERGE=$2
if [ "$MERGE" = "1" ]; then
	AUTO="and Merging"
else 
	AUTO=""
fi

# Create new pull request and get its ID
echo "Creating PR: $BITBUCKET_BRANCH -> $DEST_BRANCH $AUTO"
echo "Owner: $BITBUCKET_REPO_OWNER"
echo "Repo: $BITBUCKET_REPO_SLUG"

# Hack I know but will review later
access=`curl -X POST -u "5bTUFHnPAK6Ke32Y3M:Z8FPgXdqTjwQ6NQCzhJ5qD9XXcUsn3Zj" \
  https://bitbucket.org/site/oauth2/access_token \
  --fail --show-error --silent \
  -d grant_type=client_credentials | jq --raw-output '.access_token'`

echo "Access Token: $access"

reviewers=`curl -X GET https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/default-reviewers \
  --fail --show-error --silent \
  -H 'content-type: application/json' \
  -H 'authorization: Bearer '$access \
  | cut -d '[' -f 2 | cut -d ']' -f 1`

echo "Reviewers: $reviewers"

request='{"title": "'$BITBUCKET_BRANCH' -> '$DEST_BRANCH'","description": "Automatic Pull Request from pipelines","state": "OPEN",
    "destination": {"branch": {"name": "'$DEST_BRANCH'"}},"source": {"branch": {"name": "'$BITBUCKET_BRANCH'"}},
    "reviewers": '$reviewers' }'
	
PR_ID=`curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests \
  --fail --show-error --silent \
  -H 'content-type: application/json' \
  -H 'authorization: Bearer '$access \
  -d "$request" | jq --raw-output '.id'`

# Merge PR
if [ "$MERGE" = "1" ]; then
	echo "Merging PR: $PR_ID"
	curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests/$PR_ID/merge \
	  --fail --show-error --silent \
	  -H 'content-type: application/json' \
	  -H 'authorization: Bearer '$access \
	  -d '{
	    "close_source_branch": false,
	    "merge_strategy": "merge_commit"
	  }'
fi