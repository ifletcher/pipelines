Class Tests.Unit.DB.Reports.PharmacologistTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Class Methods ***/
Method TestGetDrugDisplayList1()
{
	#dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()

	set actual = ##class(DB.Reports.Pharmacologist).GetDrugDisplayList(patientObj)
    set expected = ""
	do ..AssertEquals(expected, actual, "An empty drug list should return empty string.")
}

Method TestGetDrugDisplayList2()
{
	#dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do patientObj.PatientDrug.Insert(..CreatePatientDrug("Placebo"))
    do patientObj.PatientDrug.Insert(..CreatePatientDrug("Sugar Pills"))

	set actual = ##class(DB.Reports.Pharmacologist).GetDrugDisplayList(patientObj)
    set expected = $listbuild("Placebo", "Sugar Pills")
	do ..AssertEquals(expected, actual, "Should return a list of the medication descriptions.")
}

Method CreatePatientDrug(code As %String) As DB.PatientDrug
{
	set drug = ##class(DB.PatientDrug).%New()
	set drug.Medication = ..CreateMedication(code, code)
	quit drug
}

Method CreateMedication(code As %String, description As %String) As DB.System.Medication
{
	set medication = ##class(DB.System.Medication).%New()
	set medication.Code = code
	set medication.Description = description
	quit medication
}

}
