Class Tests.Unit.DB.Reports.AdverseEvents.SDQCTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB.Reports.AdverseEvents:Tests.Unit.DB.Reports.AdverseEvents.SDQCTests", "/nodelete")

/*** Testing Class Methods ***/
Method TestReportHeader()
{
	set outputFile = ##class(Tests.Unit.DB.Reports.AdverseEvents.SDQCStub).Report(54321)
    do $$$AssertNotTrue(outputFile.%IsNull(), "Sanity check. A file should always be generated.")
    set actual = outputFile.ReadLine()
	set expected = "PatientID,Patient Identifiers,Project,HCP Name,Creator,Last Modified User,Date Initiated,Time Initiated,Call Type,Note Type,Notes,Awareness Date, Read Receipt Date, Reported By,Case Number,Event Details,"
	do $$$AssertEquals(expected, actual, "Expected header row: "_expected_" Actual: "_actual)
}

Method TestFormatIdentifiers1()
{
	set dob = 51620  // May 1, 1982
	set actual = ##class(Tests.Unit.DB.Reports.AdverseEvents.SDQCStub).FormatIdentifiers("BT", "M", dob)
	set expected = "BT/39/M/01-05-1982"
	do ..AssertEquals(expected, actual, "Birth date before current day.")
}

Method TestFormatIdentifiers2()
{
	set dob = 51630  // May 11, 1982
	set actual = ##class(Tests.Unit.DB.Reports.AdverseEvents.SDQCStub).FormatIdentifiers("ZF", "F", dob)
	set expected = "ZF/39/F/11-05-1982"
	do ..AssertEquals(expected, actual, "Birth date after current day")
}

Method TestFormatIdentifiers3()
{
	set dob = 50890  // May 1, 1980
	set actual = ##class(Tests.Unit.DB.Reports.AdverseEvents.SDQCStub).FormatIdentifiers("MM", "U", dob)
	set expected = "MM/41/U/01-05-1980"
	do ..AssertEquals(expected, actual, "Birth date in different year")
}

}
