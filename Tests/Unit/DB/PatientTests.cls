Class Tests.Unit.DB.PatientTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB:Tests.Unit.DB.PatientTests", "/nodelete")

/*** Testing Instance Methods ***/
Method TestRWEFlagChangeV1()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do $$$AssertEquals("", patientObj.RWEConsentLastEnabled, "Sanity check: RWEConsentLastEnabled starts blank.")
    do patientObj.UpdateConsentLastEnabledWrapper(0, "RWEConsent", "RWEConsentLastEnabled")
    do $$$AssertEquals("", patientObj.RWEConsentLastEnabled, "Consent flag staying false: RWEConsentLastEnabled unchanged.")
}

Method TestRWEFlagChangeV2()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do $$$AssertEquals("", patientObj.RWEConsentLastEnabled, "Sanity check: RWEConsentLastEnabled starts blank.")
    set patientObj.RWEConsent = 1
    do patientObj.UpdateConsentLastEnabledWrapper(0, "RWEConsent", "RWEConsentLastEnabled")
    set expectedH = $horolog
    do $$$AssertNotEquals("", patientObj.RWEConsentLastEnabled, "Consent flag changing to true, RWEConsentLastEnabled should be valued.")

    set actualTime = $piece($zdth(patientObj.RWEConsentLastEnabled, 3), ",", 2)
    do ..AssertEqualsWithinDelta($p(expectedH, ",", 2), actualTime, 1, "Consent flag changing to true, RWEConsentLastEnabled should be set to current time.")
}

Method TestRWEFlagChangeV3()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do $$$AssertEquals("", patientObj.RWEConsentLastEnabled, "Sanity check: RWEConsentLastEnabled starts blank.")
    set testTime = "65689,40000"
    set patientObj.RWEConsentLastEnabled = $zdatetime(testTime, 3)
    set patientObj.RWEConsent = 1
    do patientObj.UpdateConsentLastEnabledWrapper(1, "RWEConsent", "RWEConsentLastEnabled")
    do $$$AssertEquals("2020-11-06 11:06:40", patientObj.RWEConsentLastEnabled, "Consent flag staying true: RWEConsentLastEnabled unchanged.")
}

Method TestRWEFlagChangeV4()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    set testTime = "65689,40000"
    set patientObj.RWEConsentLastEnabled = $zdatetime(testTime, 3)
    set patientObj.RWEConsent = 0
    do patientObj.UpdateConsentLastEnabledWrapper(1, "RWEConsent", "RWEConsentLastEnabled")
    do $$$AssertEquals("", patientObj.RWEConsentLastEnabled, "Consent flag changing to true, RWEConsentLastEnabled should be cleared.")
}

/*** Testing Instance Methods ***/
Method TestStudyFlagChangeV1()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do $$$AssertEquals("", patientObj.StudyConsentLastEnabled, "Sanity check: StudyConsentLastEnabled starts blank.")
    do patientObj.UpdateConsentLastEnabledWrapper(0, "StudyConsent", "StudyConsentLastEnabled")
    do $$$AssertEquals("", patientObj.StudyConsentLastEnabled, "Consent flag staying false: StudyConsentLastEnabled unchanged.")
}

Method TestStudyFlagChangeV2()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do $$$AssertEquals("", patientObj.StudyConsentLastEnabled, "Sanity check: StudyConsentLastEnabled starts blank.")
    set patientObj.StudyConsent = 1
    do patientObj.UpdateConsentLastEnabledWrapper(0, "StudyConsent", "StudyConsentLastEnabled")
    set expectedH = $horolog
    do $$$AssertNotEquals("", patientObj.StudyConsentLastEnabled, "Consent flag changing to true, StudyConsentLastEnabled should be valued.")

    set actualTime = $piece($zdth(patientObj.StudyConsentLastEnabled, 3), ",", 2)
    do ..AssertEqualsWithinDelta($p(expectedH, ",", 2), actualTime, 1, "Consent flag changing to true, StudyConsentLastEnabled should be set to current time.")
}

Method TestStudyConsentFlagChangeV3()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    do $$$AssertEquals("", patientObj.StudyConsentLastEnabled, "Sanity check: StudyConsentLastEnabled starts blank.")
    set testTime = "65689,40000"
    set patientObj.StudyConsentLastEnabled = $zdatetime(testTime, 3)
    set patientObj.StudyConsent = 1
    do patientObj.UpdateConsentLastEnabledWrapper(1, "StudyConsent", "StudyConsentLastEnabled")
    do $$$AssertEquals("2020-11-06 11:06:40", patientObj.StudyConsentLastEnabled, "Consent flag staying true: StudyConsentLastEnabled unchanged.")
}

Method TestStudyConsentFlagChangeV4()
{
    #dim patientObj As DB.Patient
    set patientObj = ##class(Tests.Unit.DB.PatientStub).%New()
    set testTime = "65689,40000"
    set patientObj.StudyConsentLastEnabled = $zdatetime(testTime, 3)
    set patientObj.StudyConsent = 0
    do patientObj.UpdateConsentLastEnabledWrapper(1, "StudyConsent", "StudyConsentLastEnabled")
    do $$$AssertEquals("", patientObj.StudyConsentLastEnabled, "Consent flag changing to true, StudyConsentLastEnabled should be cleared.")
}

}
