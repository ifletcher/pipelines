Class Tests.Unit.Tasks.MSGAU.KesimptaStub Extends Tasks.MSGAU.Kesimpta
{

Parameter DEBUG = 0;

ClassMethod GetProject() As DB.System.Project
{
	set project = ##class(DB.System.Project).%New()
	set project.DefaultOrderRecipientEmail = "default.order.recipient.email@rxmxcorp.com"
	return project
}

ClassMethod GetFromEmail() As %String
{
	return "noreply@rxmxcorp.com"
}

ClassMethod Now()
{
	return "65755,38545"
}

ClassMethod FindMostRecentShipment(patOBJ As DB.Patient)
{
	return ""
}

ClassMethod GetDispatchedTime(shipId)
{
	return ""
}

}
