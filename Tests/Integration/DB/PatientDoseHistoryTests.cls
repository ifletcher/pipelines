Class Tests.Integration.DB.PatientDoseHistoryTests Extends %UnitTest.TestCase
{

// d ##class(%UnitTest.Manager).RunTest("Integration.DB:Tests.Integration.DB.PatientDoseHistoryTests", "/nodelete")

/*** Testing Class Methods ***/
Method TestGetDaysNoPatient()
{
    set firstPass = ""
    set actual = ##class(DB.PatientDoseHistory).CountDays("Bogus", "bogus", "bogus", .firstPass)
    do $$$AssertEquals("", firstPass, "Sanity Check: 'firstPass' is null.")
    do $$$AssertNotTrue(actual, "If invalid patient is supplied, CountDays() should be False.")
}

/*** Testing Instance Methods ***/
Method TestGetCompletedAndErrorStatuses()
{
    set ignoreStatusesList = ##class(DB.PatientDoseHistory).GetCompletedAndErrorStatuses()
    do $$$AssertNotEquals("", ignoreStatusesList, "Sanity Check: List is not null.")
    do $$$AssertTrue( ($length(ignoreStatusesList, ",") > 2), "Sanity Check: List length is greater than 2.")
}

}
