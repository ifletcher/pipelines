Class Tests.Unit.Tasks.MSGAU.KesimptaTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.Tasks.MSGAU:Tests.Unit.Tasks.MSGAU.KesimptaTests", "/nodelete")

/*** Testing Class Methods ***/
Method TestShipmentNotificationType()
{
	set actual = ##class(Tasks.MSGAU.Kesimpta).#SHIPMENTTYPE
	set expected = "10"
	do ..AssertEquals(expected, actual)
}

Method TestGetFileName()
{
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaStub).GetFileName("Kesimpta-Placebo")
	set expected = "Kesimpta-Placebo_20210111.csv"
	do ..AssertEquals(expected, actual)
}

/// Query Filter #1
Method TestPatientIsNotActiveNoRecord()
{
	set actual = ##class(Tasks.MSGAU.Kesimpta).PatientIsNotActive("")
	set expected = 1
	do ..AssertEquals(expected, actual)
}

/// Query Filter #1
Method TestPatientIsNotActiveNoTOS()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set actual = ##class(Tasks.MSGAU.Kesimpta).PatientIsNotActive(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

/// Query Filter #1
Method TestPatientIsNotActiveIsDiscontinued()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.Discontinued = 1
	set patient.InactivePatient = 0
	set patient.TOSAcceptanceTimestamp = $h
	set actual = ##class(Tasks.MSGAU.Kesimpta).PatientIsNotActive(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

/// Query Filter #1
Method TestPatientIsNotActiveIsInactive()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.Discontinued = 0
	set patient.InactivePatient = 1
	set patient.TOSAcceptanceTimestamp = $h
	set actual = ##class(Tasks.MSGAU.Kesimpta).PatientIsNotActive(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

/// Query Filter #1
Method TestPatientIsNotActiveFails()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.Discontinued = 0
	set patient.InactivePatient = 0
	set patient.TOSAcceptanceTimestamp = $h
	set actual = ##class(Tasks.MSGAU.Kesimpta).PatientIsNotActive(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

/// Query Filter #2.1:
/// No Ship to preference
Method TestPharmacyNotAcceptingShipments1()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set actual = ##class(Tasks.MSGAU.Kesimpta).PharmacyNotAcceptingShipments(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

/// Query Filter #2.2:
/// ShipToPreference is not "Pharmacy"
Method TestPharmacyNotAcceptingShipments2()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.ShipToPreference = ..GetShipToPreference("PRAC")
	set actual = ##class(Tasks.MSGAU.Kesimpta).PharmacyNotAcceptingShipments(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

/// Query Filter #2.3:
/// ShipToPreference is "Pharmacy", but no pharm record
Method TestPharmacyNotAcceptingShipments3()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.ShipToPreference = ..GetShipToPreference("PHARM")
	set actual = ##class(Tasks.MSGAU.Kesimpta).PharmacyNotAcceptingShipments(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

/// Query Filter #2.4:
/// ShipToPreference is "Pharmacy", Pharmacy not accepting
Method TestPharmacyNotAcceptingShipments4()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.ShipToPreference = ..GetShipToPreference("PHARM")
	set patient.CurrentPharmacy = ##class(DB.System.Pharmacy).%New()
	set patient.CurrentPharmacy.AcceptingShipments = "N"
	set actual = ##class(Tasks.MSGAU.Kesimpta).PharmacyNotAcceptingShipments(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

/// Query Filter #2.5:
/// ShipToPreference is "Pharmacy", Pharmacy not accepting
Method TestPharmacyNotAcceptingShipments5()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.ShipToPreference = ..GetShipToPreference("PHARM")
	set patient.CurrentPharmacy = ##class(DB.System.Pharmacy).%New()
	set patient.CurrentPharmacy.AcceptingShipments = "Y"
	set actual = ##class(Tasks.MSGAU.Kesimpta).PharmacyNotAcceptingShipments(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

/// Helper method
Method GetShipToPreference(code)
{
	set record = ##class(DB.System.ShipToPreference).%New()
	set record.Code = code
	return record
}

/// If no Current order Item, cannot submit order
Method TestCanReorderNoShipmentFrequency1()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaStub).CanReorder(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

/// If no shipment frequency on the Current order Item, cannot order
Method TestCanReorderNoShipmentFrequency2()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.CurrentOrderItem = ##class(DB.System.OrderItem).%New()
	//set patient.CurrentOrderItem.ShipmentFrequency = ""
	set patient.CurrentOrderItem.Description = "No Dispatch Date"
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaMock).CanReorder(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

Method TestCanReorderNoShipment()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.CurrentOrderItem = ##class(DB.System.OrderItem).%New()
	set patient.CurrentOrderItem.ShipmentFrequency = 10
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaStub).CanReorder(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

Method TestCanReorderNoDispatchDate()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.CurrentOrderItem = ##class(DB.System.OrderItem).%New()
	set patient.CurrentOrderItem.ShipmentFrequency = 10
	set patient.CurrentOrderItem.Description = "No Dispatch Date"
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaMock).CanReorder(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

Method TestCanReorderRecentDispatchDate()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.CurrentOrderItem = ##class(DB.System.OrderItem).%New()
	set patient.CurrentOrderItem.ShipmentFrequency = 10
	set patient.CurrentOrderItem.Description = "Recent Dispatch Date"
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaMock).CanReorder(patient)
	set expected = 0
	do ..AssertEquals(expected, actual)
}

Method TestCanReorderOldDispatchDate()
{
	set patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set patient.CurrentOrderItem = ##class(DB.System.OrderItem).%New()
	set patient.CurrentOrderItem.ShipmentFrequency = 10
	set patient.CurrentOrderItem.Description = "Old Dispatch Date"
	set actual = ##class(Tests.Unit.Tasks.MSGAU.KesimptaMock).CanReorder(patient)
	set expected = 1
	do ..AssertEquals(expected, actual)
}

}
