Class Tests.Unit.DB.Reports.Shipments.ShipmentListTests Extends Tests.AbstractTestCase
{

/*** Testing Class Methods ***/
Method TestReportHeader()
{
	set outputFile = ##class(DB.Reports.Shipments.ShipmentList).CreateFile()
    do $$$AssertNotTrue(outputFile.%IsNull(), "Sanity check. A file should always be generated.")
    set actual = outputFile.ReadLine()
	set expected = "Order Date,Customer Number,RXMX #,Pharmacy approval to participate and supply PFP,Order Type,Pharmacy Name,Delivery Address 1,Delivery Address 2,Delivery City,Del State,Del Postcode,Pharmacy Contact Name,Pharmacy Contact Number,Manufacture Code,SKU#,QTY,Status,Total $ (excluding GST),Total $ (including GST),Patient ID,Patient Name,Patient Shipping Instructions"
	do ..AssertEquals(expected, actual)
}

Method TestQueryCompile1()
{
	set query = ##class(DB.Reports.Shipments.ShipmentList).GetQuery(0)
    set rs=##class(%ResultSet).%New()
    set tSC=rs.Prepare(query)
	do $$$AssertStatusOK(tSC, "Query Should compile")
}

Method TestQueryCompile2()
{
	set query = ##class(DB.Reports.Shipments.ShipmentList).GetQuery(1)
    set rs=##class(%ResultSet).%New()
    set tSC=rs.Prepare(query)
	do $$$AssertStatusOK(tSC, "Query Should compile")
}

}
