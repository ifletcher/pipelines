Class Tests.AbstractTestCase Extends %UnitTest.TestCase
{

/// Wrapper plus some.
Method AssertEquals(expected, actual, description = "")
{
	if (expected = actual) {
		do $$$AssertSuccess("AssertEquals "_description)
	}
	else {
		do $$$AssertFailure("AssertEquals "_description_"  Expected: "_expected_" Actual: "_actual)
	}
}

Method AssertIsEmptyString(actual, description = "")
{
	set message = description
	if (actual = "") {
		do $$$AssertSuccess("AssertIsEmptyString "_description)
	}
	else {
		do $$$AssertFailure("AssertIsEmptyString "_description _ " Expected: (empty string) Actual: "_actual)
	}
}

/// sample usage: <br>
/// do ..AssertEqualsWithinDelta($p(dataObj.LastPWChange,",",2), $p(currentDateTime,",",2), 1, "Property should be set to current time.")
Method AssertEqualsWithinDelta(expected, actual, delta, description = "")
{
	set difference = $zabs(expected - actual)
	if (difference <= delta) {
		do $$$AssertSuccess("AssertEqualsWithinDelta "_description)
	}
	else {
		do $$$AssertFailure("AssertEqualsWithinDelta "_description_" Value falls outside acceptible delta (+-"_delta_")")
	}
}

}
