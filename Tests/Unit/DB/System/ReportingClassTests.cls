Class Tests.Unit.DB.System.ReportingClassTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Class Methods ***/
Method TestGetClassTypeByClassName()
{
    set codeTable = "DB.System.Locale"
    set classType = ##class(DB.System.ReportingClass).GetClassTypeByClassName(codeTable)
    do ..AssertEquals("CodeTable", classType, "Class type should be code table")
    set codeTable = "DB.Questionnaires"
    set classType = ##class(DB.System.ReportingClass).GetClassTypeByClassName(codeTable)
    do ..AssertEquals("TransactionTable", classType, "Class type should be transaction table")
    set codeTable = "DB.System.Log.AccessLog"
    set classType = ##class(DB.System.ReportingClass).GetClassTypeByClassName(codeTable)
    do ..AssertEquals("LogTable", classType, "Class type should be log table")
    set codeTable = "DB.System.Log.Audit.DB.DrugShipmentRequest"
    set classType = ##class(DB.System.ReportingClass).GetClassTypeByClassName(codeTable)
    do ..AssertEquals("AuditTable", classType, "Class type should be aduit table")
}

}
