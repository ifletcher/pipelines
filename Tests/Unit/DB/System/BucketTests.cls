Class Tests.Unit.DB.System.BucketTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Instance Methods ***/
Method TestGetSQLConditionsBase()
{
	set bucket = ##class(DB.System.Bucket).%New()
	set whereClause = bucket.GetSQLConditions()
	do ..AssertIsEmptyString(whereClause, "No conditions should produce a blank where clause.")
}

Method TestGetSQLConditionsNotEqualsConversion()
{
	set quote = $c(34)
	set bucket = ##class(DB.System.Bucket).%New()
	do bucket.BucketConditions.Insert(..CreateCondition("Notification.Recipient", "'=", quote_"foo@example.com"_quote))
	set actual = bucket.GetSQLConditions()
	set expected = "WHERE ( ""Notification""->""Recipient"" <> 'foo@example.com' )"
	do ..AssertEquals(expected, actual, "Not equals ('=) should be converted to SQL equivalent.")
}

/*
Method TestGetSQLConditionsDateConditions()
{
	set bucket = ##class(DB.System.Bucket).%New()
	do bucket.BucketConditions.Insert(..CreateCondition("DateCreated", "<", "DATEADD(D,14,CURRENT_DATE)"))
	
	set actual = bucket.GetSQLConditions()
	set expected = "WHERE ( ""DateCreated"" < DATEADD(D,14,CURRENT_DATE) )"
	do ..AssertEquals(expected, actual, "DATEADD SQL method should not be escaped.")
}
*/
Method TestGetSQLConditionsMultipleConditions()
{
	set bucket = ##class(DB.System.Bucket).%New()
	do bucket.BucketConditions.Insert(..CreateCondition("SendVia", "=", 3, "&&"))
	do bucket.BucketConditions.Insert(..CreateCondition("Completed", "=", 0))
	
	set actual = bucket.GetSQLConditions()
	set expected = "WHERE ( ""SendVia"" = '3' ) AND ( ""Completed"" = '0' )"
	do ..AssertEquals(expected, actual, "Compound where clause should be joined by AND.")
}

Method TestGetSQLConditionsAbusiveCondition1()
{
	set bucket = ##class(DB.System.Bucket).%New()
	do bucket.BucketConditions.Insert(..CreateCondition("1", "=", "1) UNION ALL SELECT FullName FROM DB.Patient --"))
	set actual = bucket.GetSQLConditions()
	set expected = "WHERE ( ""1"" = '1) UNION ALL SELECT FullName FROM DB.Patient --' )"
	do ..AssertEquals(expected, actual, "Value should be wrapped in single quotes to avoid abuse.")
}

Method TestGetSQLConditionsAbusiveCondition2()
{
	set bucket = ##class(DB.System.Bucket).%New()
	do bucket.BucketConditions.Insert(..CreateCondition("Message", "=", "O'Mally"))
	set actual = bucket.GetSQLConditions()
	set expected = "WHERE ( ""Message"" = 'O''Mally' )"
	do ..AssertEquals(expected, actual, "Single quotes should be escaped.")
}

Method TestGetSQLConditionsEmptyStringConditions()
{
	set bucket = ##class(DB.System.Bucket).%New()
	do bucket.BucketConditions.Insert(..CreateCondition("Name", "=", "", "||"))
	do bucket.BucketConditions.Insert(..CreateCondition("Number", "'=", ""))

	set actual = bucket.GetSQLConditions()
	set expected = "WHERE ( ""Name"" IS NULL ) OR ( ""Number"" IS NOT NULL )"
	do ..AssertEquals(expected, actual, "Return IS NULL or IS NOT NULL when value = or '= ''")
}

Method CreateCondition(field, comparison, value, logicalOperand = "") As DB.System.BucketConditions
{
	set condition = ##class(DB.System.BucketConditions).%New()
	set condition.Field = field
	set condition.Condition = comparison
	set condition.Value = value
	set condition.Continue = logicalOperand
	quit condition
}

}
