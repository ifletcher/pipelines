Class Tests.Unit.DB.PatientStub Extends DB.Patient
{

Method %OnNew() As %Status [ Private, ServerOnly = 1 ]
{
	quit $$$OK
}

Method UpdateConsentLastEnabledWrapper(oldValue As %Boolean, pConsentProperty As %String = "", pConsentEnabledProperty As %String = "")
{
	do ..UpdateConsentLastEnabled(oldValue, pConsentProperty, pConsentEnabledProperty)
}

Storage Default
{
<Type>%Storage.Persistent</Type>
}

}
