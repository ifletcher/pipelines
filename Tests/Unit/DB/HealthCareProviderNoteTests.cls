Class Tests.Unit.DB.HealthCareProviderNoteTests Extends %UnitTest.TestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit:Tests.Unit.DB.HealthCareProviderNoteTests", "/nodelete")

/*** Testing Instance Methods ***/
Method TestSetTimeCreated()
{
    #dim noteObj As DB.HealthCareProviderNote
    set noteObj = ##class(DB.HealthCareProviderNote).%New()
    do $$$AssertEquals("", noteObj.TimeCreated, "Sanity check: TimeCreated starts blank.")
    set now = $horolog
    do noteObj.SetTimeCreated()
	set actual = noteObj.TimeCreated
	set expected = $zdatetime(now, 3)
    set MaxTimeDifference = 1  ; second
    do ..AssertEqualsWithinDelta($piece(now, ",", 2), $piece($zdth(actual, 3), ",", 2), MaxTimeDifference, "TimeCreated should be 'Now'.")
}

Method TestSetTimeCreatedAlreadyValued()
{
    #dim noteObj As DB.HealthCareProviderNote
    set noteObj = ##class(DB.HealthCareProviderNote).%New()
    set noteObj.TimeCreated = $zdatetime($zdth("2020-10-19 09:40:00", 3),3)
    do noteObj.SetTimeCreated()
	set expected = "2020-10-19 09:40:00"
	set actual = noteObj.TimeCreated
	do $$$AssertEquals(expected, actual, "TimeCreated should not reset.")
}

/*** Testing Class Methods ***/
Method TestGetNoteListNoHCP()
{
    set actual = ##class(DB.HealthCareProviderNote).GetNoteIDsByHCP("")
	set expected = ""
	do $$$AssertEquals(expected, actual, "No HCP Id should return empty string.")
}

Method TestGetNoteListNoNotes()
{
    set actual = ##class(DB.HealthCareProviderNote).GetNoteIDsByHCP("InvalidID")
	set expected = ""
	do $$$AssertEquals(expected, actual, "No HCP Notes should return empty string.")
}

/// sample usage: <br>
/// do ..AssertEqualsWithinDelta($p(dataObj.LastPWChange,",",2), $p(currentDateTime,",",2), 1, "Property should be set to current time.")
Method AssertEqualsWithinDelta(expected, actual, delta, description = "")
{
	set difference = $zabs(expected - actual)
	if (difference <= delta) {
		do $$$AssertSuccess(description)
	}
	else {
		do $$$AssertFailure(description_" Value falls outside acceptible delta (+-"_delta_")")
	}
}

}
