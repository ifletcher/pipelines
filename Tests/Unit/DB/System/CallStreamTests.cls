Class Tests.Unit.DB.System.CallStreamTests Extends Tests.AbstractTestCase
{

/*** Testing Instance Methods ***/
Method TestGetNextCallStreamCalculationNoCalculations()
{
	set callStreamObj = ..GetCallStream()
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(0)
    do ..AssertIsEmptyString(nextCalculationRecord, "No calculations on a stream should not return a calculation object.")
}

Method TestGetNextCallStreamCalculationMultipleInProgressCalculations()
{
	set callStreamObj = ..GetCallStream(1)
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(0, 0)
	do ..AssertEquals(1, nextCalculationRecord.CallNumber, "Initial call number should be 1")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(1, 0)
	do ..AssertEquals(1, nextCalculationRecord.CallNumber, "Current call number should be 1")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(2, 0)
	do ..AssertEquals(2, nextCalculationRecord.CallNumber, "Current call number should be 2")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(3, 0)
	do ..AssertEquals(3, nextCalculationRecord.CallNumber, "Current call number should be 3")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(4, 0)
	do ..AssertIsEmptyString(nextCalculationRecord, "Call number is missing from sequence and should not return a record.")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(7, 0)
	do ..AssertEquals(7, nextCalculationRecord.CallNumber, "Current call number should be 7")
}

Method TestGetNextCallStreamCalculationMultipleCompletedCalculations()
{
	set callStreamObj = ..GetCallStream(1)
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(0, 1)
	do ..AssertEquals(1, nextCalculationRecord.CallNumber, "Initial call number should be 1")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(1, 1)
	do ..AssertEquals(2, nextCalculationRecord.CallNumber, "Next call number should be 2")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(2, 1)
	do ..AssertEquals(3, nextCalculationRecord.CallNumber, "Next call number should be 3")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(3, 1)
	do ..AssertEquals(7, nextCalculationRecord.CallNumber, "Next call number should be 7")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(4, 1)
	do ..AssertEquals(7, nextCalculationRecord.CallNumber, "Call gap should return next available in sequence (7)")
	//
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(7, 1)
	do ..AssertIsEmptyString(nextCalculationRecord, "There is no call after the last call.")
}

Method TestGetNextCallStreamCalculationMultipleCalculationsInvalidCallNumber()
{
	set callStreamObj = ..GetCallStream(1)
	set nextCalculationRecord = callStreamObj.GetNextCallStreamCalculation(99, 1)
	do ..AssertIsEmptyString(nextCalculationRecord, "Call number provided is higher than last call number should not return a record.")
}

ClassMethod GetCallStream(fillCalculations As %Boolean = 0) As DB.System.CallStream
{
    set callStream = ##class(DB.System.CallStream).%New()
	set (callStream.Code, callStream.Description) = "High"
	if fillCalculations {
		do callStream.CallStreamCalculations.Insert(..CreateCalculationRecord(3))
		do callStream.CallStreamCalculations.Insert(..CreateCalculationRecord(1))
		do callStream.CallStreamCalculations.Insert(..CreateCalculationRecord(2))
		do callStream.CallStreamCalculations.Insert(..CreateCalculationRecord(7))
	}
    quit callStream
}

ClassMethod CreateCalculationRecord(callNumber = 1) As DB.System.CallStreamCalculations
{
	set calculationRecord = ##class(DB.System.CallStreamCalculations).%New()
	set (calculationRecord.CallNumber, calculationRecord.DaysToNextCall) = callNumber
	set calculationRecord.CalculatedFrom = "WelcomeCallDate"
	quit calculationRecord
}

}
