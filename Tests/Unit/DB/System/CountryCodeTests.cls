Class Tests.Unit.DB.System.CountryCodeTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Class Methods ***/
Method TestGetNumericCodemNoCode()
{
    set actual = ##class(DB.System.CountryCode).GetNumericCode("")
    do ..AssertIsEmptyString(actual, "A null/empty country code should return empty string.")
    do ..AssertIsEmptyString($get(%objlasterror), "%objlasterror should not be valued.")
}

}
