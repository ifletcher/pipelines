Class Tests.Unit.DB.HealthCareProviderTeamTests Extends Tests.AbstractTestCase
{

/*** Testing Class Methods ***/
Method TestValidateDelegateAccessNullPatientId()
{
    set team = ##class(DB.HealthCareProviderTeam).%New()
    set actual = team.ValidateDelegatePatientAccess("")
    set expected = 0
    do ..AssertEquals(expected, actual, "No Patient Id should return 0-No Access.")
}

Method TestValidateDelegateAccessNullTeamUser()
{
    set team = ##class(DB.HealthCareProviderTeam).%New()
    do ..AssertEquals("", team.TeamUser, "Sanity Check: TeamUser should be null.")
    set actual = team.ValidateDelegatePatientAccess("99999")
    set expected = 0
    do ..AssertEquals(expected, actual, "No TeamUser should return 0-No Access.")
}

}