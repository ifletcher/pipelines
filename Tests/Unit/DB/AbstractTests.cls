Class Tests.Unit.DB.AbstractTests Extends Tests.AbstractTestCase
{

Method TestListRemoveElementFromNullList()
{
    set actual = ##class(DB.Abstract).ListRemoveElement("", "orange")
    set expected = ""
    do ..AssertEquals(expected, actual, "Null List Identity check.")
}

Method TestListRemoveElementFound()
{
    set fieldList = $listbuild("red", "orange", "blue")
    set actual = ##class(DB.Abstract).ListRemoveElement(fieldList, "orange")
    set expected = $listbuild("red", "blue")
    do ..AssertEquals(expected, actual, "Should remove element if found")
}

Method TestListRemoveElementNotFound()
{
    set fieldList = $listbuild("red", "orange", "blue")
    set actual = ##class(DB.Abstract).ListRemoveElement(fieldList, "green")
    set expected = $listbuild("red", "orange", "blue")
    do ..AssertEquals(expected, actual, "List should be unchanged.")
}

}
