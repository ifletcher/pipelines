Class Tests.Unit.DB.System.QuestionnairesTests Extends Tests.AbstractTestCase
{

/*** Testing Instance Methods ***/
Method TestGetFormResponsePropertiesNoQuestions()
{
    set questionnaire = ..CreateQuestionnaire()
    set actual = ""
    do questionnaire.GetFormResponseProperties(.actual)
    do ..AssertEquals(0, actual.Count(), "Base Case: no questions produce empty pOptions.")
}

Method TestGetFormResponsePropertiesLayoutMissingQuestion()
{
    set questionnaire = ..CreateQuestionnaire()
    set layout = ##class(DB.System.QuestionLayout).%New()
    do questionnaire.QuestionLayout.Insert(layout)
    set actual = ""
    do questionnaire.GetFormResponseProperties(.actual)
    do $$$AssertSuccess("GetFormResponseProperties() should not throw an error if a QuestionLayout record is missing a question.")
    do ..AssertEquals(0, actual.Count(), "Nothing to add here.")
}

Method TestGetFormResponsePropertiesOneQuestionNewTable()
{
    set questionnaire = ..CreateQuestionnaire()
    do questionnaire.QuestionLayout.Insert(..CreateLayout("DB.Test.Cars", "Color"))
    set actual = ""
    do questionnaire.GetFormResponseProperties(.actual)
    do ..AssertEquals(0, actual.Count(), "If the table isn't in pOptions, do not add. (because that translates to all properties)")
}

Method TestGetFormResponsePropertiesOneQuestionExistingTable()
{
    set questionnaire = ..CreateQuestionnaire()
    do questionnaire.QuestionLayout.Insert(..CreateLayout("DB.Test.Cars", "Color"))
    set actual = ##class(%ArrayOfDataTypes).%New()
    do actual.SetAt($lb("Make"), "DB.Test.Cars")
    do questionnaire.GetFormResponseProperties(.actual)
    do ..AssertEquals(1, actual.Count(), "Sanity check: table exists.")
    do ..AssertEquals($lb("Make", "Color"), actual.GetAt("DB.Test.Cars"), "Table exists, property added.")
}

Method TestGetFormResponsePropertiesOneQuestionExistingProperty()
{
    set questionnaire = ..CreateQuestionnaire()
    do questionnaire.QuestionLayout.Insert(..CreateLayout("DB.Test.Cars", "Color"))
    set actual = ##class(%ArrayOfDataTypes).%New()
    do actual.SetAt($lb("Color"), "DB.Test.Cars")
    do questionnaire.GetFormResponseProperties(.actual)
    do ..AssertEquals(1, actual.Count(), "Check table count.")
    do ..AssertEquals($lb("Color"), actual.GetAt("DB.Test.Cars"), "Property exists, it is not duplicated.")
}

Method TestGetFormResponsePropertiesGoogleAddressType()
{
    set questionnaire = ..CreateQuestionnaire()
    do questionnaire.QuestionLayout.Insert(..CreateLayout("DB.Test.AnythingButPatient", "Email", "app-google-address"))
    set actual = ##class(%ArrayOfDataTypes).%New()
    do actual.SetAt($lb("Address"), "DB.Patient")
    do questionnaire.GetFormResponseProperties(.actual)
    do ..AssertEquals(1, actual.Count(), "Sanity Check table count.")
    do ..AssertEquals($lb("Address", "Email"), actual.GetAt("DB.Patient"), "If DataType=app-google-address, then property is treated as DB.Patient.")
}

Method TestGetFormResponsePropertiesHCPPracticeType()
{
    set questionnaire = ..CreateQuestionnaire()
    do questionnaire.QuestionLayout.Insert(..CreateLayout("DB.Test.AnythingButPatient", "BogusProperty", "app-hcp-practice-drop-downs-grouped"))
    set actual = ##class(%ArrayOfDataTypes).%New()
    do actual.SetAt($lb("Address"), "DB.Patient")
    do questionnaire.GetFormResponseProperties(.actual)
    do ..AssertEquals(1, actual.Count(), "Sanity Check table count.")
    do ..AssertEquals($lb("Address", "HealthCareProvider", "Practice"), actual.GetAt("DB.Patient"), "If DataType=app-hcp-practice-drop-downs-grouped, then 1) property is treated as DB.Patient 2) HCP and practice are added to JSON data map.")
}

/// Limit the number of form fields included in JSON responses.
Method TestGetFormResponsePropertiesDisplayInList()
{
    set questionnaire = ..CreateQuestionnaire()
    // Custom fields in forms tables will not have a class populated.
    do questionnaire.QuestionLayout.Insert(..CreateLayout("", "Make", "%String",1))
    do questionnaire.QuestionLayout.Insert(..CreateLayout("", "Model", "%String", ""))
    do questionnaire.QuestionLayout.Insert(..CreateLayout("", "Color", "%String",1))
    do questionnaire.QuestionLayout.Insert(..CreateLayout("", "Hybrid", "%String", 0))
    set actual = ##class(%ArrayOfDataTypes).%New()
    do questionnaire.GetFormResponseProperties(.actual, 1)
    do ..AssertEquals(1, actual.Count(), "Sanity check: table exists.")
    do ..AssertEquals($lb("Status", "Make", "Color"), actual.GetAt("DB.Forms.Form.SAMPLE"), "Table exists.  Only fields marked DisplayInList are added to JSON Map.")
}

ClassMethod CreateQuestionnaire(code = "SAMPLE") As DB.System.Questionnaires
{
    set questionnaire = ##class(DB.System.Questionnaires).%New()
    set questionnaire.Code = code
    quit questionnaire
}

ClassMethod CreateLayout(className = "", property = "", dataType = "", displayInList = 0) As DB.System.QuestionLayout
{
    set layout = ##class(DB.System.QuestionLayout).%New()
    set layout.Question = ..CreateQuestion(className, property, dataType, displayInList)
    quit layout
}

ClassMethod CreateQuestion(className, property, dataType = "", displayInList = 0) As DB.System.Questions
{
    set question = ##class(DB.System.Questions).%New()
    set question.Code = $select(className = "": property, 1:"")
    set question.Name = $select(className = "": $tr(dataType,"%",""), 1: property)
    set question.ClassName = className
    set question.DataType = dataType
    set question.DisplayInList = displayInList
    return question
}

}
