Class Tests.Unit.DB.PathologyObservationTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB:Tests.Unit.DB.PathologyObservationTests", "/nodelete")

/*** Testing Class Methods ***/

/*
Method TestAlertCountWithNullInput()
{
    // Most of the callers I found check for id="".
    // If we check for this, we should add a check and throw something more specific than <UNDEFINED>
	set actual = ##class(DB.PathologyObservation).GetTotalAlertsByPatientId("")
	set expected = 0
	do $$$AssertEquals(expected, actual, "No input should return zero count.")
}
*/
Method TestAlertCountWithInvalidInput()
{
	set actual = ##class(DB.PathologyObservation).GetTotalAlertsByPatientId("NonExistant Patient ID")
	set expected = 0
	do $$$AssertEquals(expected, actual, "Non-existant PatientID should return zero count.")
}

/*** Testing FormatMsg() ***/
Method TestFormatMsgPatientTokens1()
{
	set msg = ..GetMessagePatientTokens()
	set patient = ..GetPatient()
	set expectedValue = "Bobby Tables, ,,Emmit Brown, ,,,[patientdose]"
	set actualValue = ##class(DB.PathologyObservation).FormatMsg(,,msg,,patient)
	do ..AssertEquals(expectedValue, actualValue, "Patient Tokens should be translated")
}

Method TestFormatMsgPatientTokens2()
{
	set msg = ..GetMessagePatientTokens()
	set patient = ..GetPatient("3/12/1981", "153mg", "Healthy", "Complete")
	set expectedValue = "Bobby Tables, ,12/03/1981,Emmit Brown, ,Healthy,Complete,153mg"
	set actualValue = ##class(DB.PathologyObservation).FormatMsg(,,msg,,patient)
	do ..AssertEquals(expectedValue, actualValue, "Patient Tokens should be translated")
}

Method TestFormatMsgTeamTokens1()
{
	set msg = ..GetMessageTeamTokens()
	set patient = ..GetPatient()
	set expectedValue = ",[delegatename],[delegatenumber],"
	set actualValue = ##class(DB.PathologyObservation).FormatMsg(,,msg,,patient)
	do ..AssertEquals(expectedValue, actualValue, "Delegate Tokens should not be translated.")
}

Method TestFormatMsgTeamTokens2()
{
	set msg = ..GetMessageTeamTokens()
	set patient = ..GetPatient()
	set teamObj = ..GetTeam()
	set expectedValue = ",Dr. Strangelove, ,"
	set actualValue = ##class(DB.PathologyObservation).FormatMsg(,,msg, teamObj, patient)
	do ..AssertEquals(expectedValue, actualValue, "Delegate Tokens should be translated using delegate data.")
}

/*
Method TestFormatMsgTeamTokens3()
{
	set msg = ..GetMessageTeamTokens()
	set patient = ..GetPatient()
	set teamObj = ..GetTeam()
	set expectedValue = ",Albert Einstein, ,"
	set actualValue = ##class(DB.PathologyObservation).FormatMsg(,,msg, teamObj, patient, 1)
	do ..AssertEquals(expectedValue, actualValue, "Delegate Tokens should be translated using team owner data.")
}*/

ClassMethod GetMessagePatientTokens()
{
	quit "[patientname],[patientnumber],[patientdob],[hcpname],[hcpnumber],[patientstatus],[surveystatus],[patientdose]"
}

ClassMethod GetMessageTeamTokens()
{
	quit ",[delegatename],[delegatenumber],"
}

Method GetPatient(dob = "", dose = "", patStatus = "", surveyStatus = "") As Tests.Unit.DB.PatientStub
{
    set patObj = ##class(Tests.Unit.DB.PatientStub).%New()
    set patObj.FirstName = "Bobby"
    set patObj.LastName = "Tables"
	set patObj.MobilePhone.CountryCode = ""
	set:(dob'="") patObj.DateOfBirth = $zdh(dob)
	if (dose'="") {
		set patObj.CurrentDose = ##class(DB.System.Dose).%New()
		set patObj.CurrentDose.Description = dose
	}
	if (patStatus'="") {
		set patObj.PatientStatus = ##class(DB.System.PatientStatus).%New()
		set patObj.PatientStatus.Description = patStatus
	}
	if (surveyStatus'="") {
		set patObj.SurveyStatus = ##class(DB.System.SurveyStatus).%New()
		set patObj.SurveyStatus.Description = surveyStatus
	}

	set hcpObj = ##class(DB.HealthCareProvider).%New()
	set hcpObj.FirstName = "Emmit"
	set hcpObj.LastName = "Brown"
	set hcpObj.MobilePhone.CountryCode = ""
	set patObj.HealthCareProvider = hcpObj
    quit patObj
}

Method GetTeam()
{
	// The Delegate
	set teamObj = ##class(DB.HealthCareProviderTeam).%New()
	set teamObj.TeamUser = ##class(DB.System.UserAccount).%New()
	set (teamObj.FirstName, teamObj.TeamUser.FirstName) = "Dr."
	set (teamObj.LastName, teamObj.TeamUser.LastName) = "Strangelove"
	set teamObj.MobilePhone.CountryCode = ""
	//
	// The Team owner/creator
	set teamObj.Creator = ##class(DB.System.UserAccount).%New()
	set teamObj.Creator.FirstName = "Albert"
	set teamObj.Creator.LastName = "Einstein"
	set teamObj.Creator.MobilePhone.CountryCode = ""
	quit teamObj
}

}
