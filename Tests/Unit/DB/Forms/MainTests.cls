Class Tests.Unit.DB.Forms.MainTests Extends Tests.AbstractTestCase
{

/*** Testing Class Methods ***/
Method TestSetFormResponseOptions()
{
    set actual = ""
    do ##class(DB.Forms.Main).SetFormResponseOptions(.actual)
    do ..AssertEquals(8, actual.Count(), "Default JSON schema for form responses contains 8 classes.")

    set expected = $listbuild("Code", "Description", "ClassDefinition", "FormType", "QuestionLayout", "QuestionPermissions")
    do ..AssertEquals(expected, actual.GetAt("DB.System.Questionnaires"), "Expected DB.System.Questionnaires properties.")

    set expected = $listbuild("Page", "Question")
    do ..AssertEquals(expected, actual.GetAt("DB.System.QuestionLayout"), "Expected DB.System.QuestionLayout properties.")

    set expected = $listbuild("Code", "Description", "DataType", "Type")
    do ..AssertEquals(expected, actual.GetAt("DB.System.Questions"), "Expected DB.System.Questions properties.")

    set expected = $listbuild("FullName")
    do ..AssertEquals(expected, actual.GetAt("DB.Patient"), "Expected DB.Patient properties.")

    set expected = $listbuild("FullName")
    do ..AssertEquals(expected, actual.GetAt("DB.HealthCareProvider"), "Expected DB.HealthCareProvider properties.")

    set expected = $listbuild("FullName")
    do ..AssertEquals(expected, actual.GetAt("DB.PatientDelegate"), "Expected DB.PatientDelegate properties.")

    set expected = $listbuild("Status")
    do ..AssertEquals(expected, actual.GetAt("DB.PatientAppointments"), "Expected DB.PatientAppointments properties.")

    set expected = $listbuild("FullName", "EmailAddress")
    do ..AssertEquals(expected, actual.GetAt("DB.System.UserAccount"), "Expected DB.System.UserAccount properties.")
}

}
