Class Tests.Unit.DB.Reports.Shipments.ReorderDrugTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB.Reports.Shipments:Tests.Unit.DB.Reports.Shipments.ReorderDrugTests", "/nodelete")

/*** Testing Class Methods ***/
Method TestReportHeader()
{
	set outputFile = ##class(DB.Reports.Shipments.ReorderDrug).CreateFile()
    do $$$AssertNotTrue(outputFile.%IsNull(), "Sanity check. A file should always be generated.")
    set actual = outputFile.ReadLine()
	set expected = "Order Date,RXMX #,Order Type,Delivery Location,Delivery Name,Delivery Address 1,Delivery Address 2,Delivery City,Delivery State,Delivery Postcode,Contact Number,Manufacture Code,Product Description,Phase,QTY,Patient ID,Patient Name,Shipping Instructions"
	do ..AssertEquals(expected, actual, "Validate header row")
}

Method TestQueryCompilePTask0()
{
	set query = ##class(DB.Reports.Shipments.ReorderDrug).GetQuery(0)
    set rs=##class(%ResultSet).%New()
    set tSC=rs.Prepare(query)
	do $$$AssertStatusOK(tSC, "Query Should compile")
}

Method TestQueryCompilePTask1()
{
	set query = ##class(DB.Reports.Shipments.ReorderDrug).GetQuery(1)
    set rs=##class(%ResultSet).%New()
    set tSC=rs.Prepare(query)
	do $$$AssertStatusOK(tSC, "Query Should compile")
}

Method TestSuppliedDateRangeStartTuesday()
{
	set report = ##class(Tests.Unit.DB.Reports.Shipments.ReorderDrugSpy).%New()
	set % = report.Run(65875, 65876)   // Tue 11 May 2021 - Wed 12 May 2021
	do $$$LogMessage("Base Case: Tuesday-Wednessday dates should be passed through unaltered.")
	do ..AssertEquals(65875, report.ObservedStartDate, "Verify the start date is passed to GetData() unaltered.")
	do ..AssertEquals(65876, report.ObservedEndDate, "Verify the end date is passed to GetData() unaltered")
}

Method TestSuppliedDateRangeReversed()
{
	set report = ##class(Tests.Unit.DB.Reports.Shipments.ReorderDrugSpy).%New()
	set % = report.Run(65876, 65875)   // Wed, 12 May 2021 - Tue, 11 May 2021
	do $$$LogMessage("If the end date is before the start date, they should be swapped.")
	do ..AssertEquals(65875, report.ObservedStartDate, "Verify the end date is swapped with the start date.")
	do ..AssertEquals(65876, report.ObservedEndDate, "Verify the start date is swapped with the end date.")
}

Method TestSuppliedDateRangeStartingMonday()
{
	set report = ##class(Tests.Unit.DB.Reports.Shipments.ReorderDrugSpy).%New()
	set % = report.Run(65874, 65876)   // Mon, 10 May 2021 - Wed, 12 May 2021
	do $$$LogMessage("If the start date is a Monday, push it back 3 days to the previous Friday.")
	do ..AssertEquals(65871, report.ObservedStartDate, "Verify the start date is the previous Friday.")
	do ..AssertEquals(65876, report.ObservedEndDate, "Verify the end date is unaltered.")
}

Method TestDefaultDateRangeRunWednessday()
{
	set report = ##class(Tests.Unit.DB.Reports.Shipments.ReorderDrugSpy).%New()
	set report.ReferenceDate = 65876   // Wednessday, 12 May 2021
	set % = report.Run()
	do $$$LogMessage("If the report is run on a Wednessday, default range should be (today-1) to (today).")
	do ..AssertEquals(65875, report.ObservedStartDate, "Verify the default start date is Tuesday, 11 May 2021.")
	do ..AssertEquals(65876, report.ObservedEndDate, "Verify the default end date is Wednessday, 12 May 2021.")
}

Method TestDefaultDateRangeRunMondayLOKI781()
{
	set report = ##class(Tests.Unit.DB.Reports.Shipments.ReorderDrugSpy).%New()
	set report.ReferenceDate = 65874   // Monday, 10 May 2021
	set % = report.Run()
	do $$$LogMessage("If the report is run on a Monday, the default range should be (today-3) to (today).  (Verifying LOKI-781)")
	do ..AssertEquals(65871, report.ObservedStartDate, "Verify the default start date is Friday, 7 May 2021.")
	do ..AssertEquals(65874, report.ObservedEndDate, "Verify the default end date is Monday, 10 May 2021.")
}

}
