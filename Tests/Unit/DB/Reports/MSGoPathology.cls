Class Tests.Unit.DB.Reports.MSGoPathology Extends %UnitTest.TestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB.Reports:Tests.Unit.DB.Reports.MSGoPathology", "/nodelete")

/*** Testing Class Methods ***/
Method TestGetProviderNumberReturnsRegistrationNumber()
{
	set requestObj = ..GenerateRequest("12345-Reg")
	set actual = ##class(DB.Reports.MSGoPathology).GetProviderNumber(requestObj)
	set expected = "12345-Reg"
	do $$$AssertEquals(expected, actual, "Base number should be the provider's Registration number.")
}

Method TestGetProviderNumberReturnsHCPProviderNumber()
{
	set requestObj = ..GenerateRequest("12345", "67890-HCP")
	set actual = ##class(DB.Reports.MSGoPathology).GetProviderNumber(requestObj)
	set expected = "67890-HCP"
	do $$$AssertEquals(expected, actual, "HCP's Provider number should be returned.")
}

Method TestGetProviderNumberReturnsPracticeProviderNumber()
{
	set requestObj = ..GenerateRequest("12345", "67890-HCP", "34567-Practice")
	set actual = ##class(DB.Reports.MSGoPathology).GetProviderNumber(requestObj)
	set expected = "34567-Practice"
	do $$$AssertEquals(expected, actual, "HCP's Practice specific provider number should be returned.")
}

Method TestGetAddressNoPractice()
{
	set requestObj = ..GenerateRequest("12345")
	set requestObj.Patient.Practice = ""
	set actual = ##class(DB.Reports.MSGoPathology).FormatPracticeAddress(requestObj)
	set expected = ""
	do $$$AssertEquals(expected, actual, "No Patient Practice should return empty string")
}

Method TestGetAddress1()
{
	#dim requestObj AS DB.PathologyRequest
	set requestObj = ..GenerateRequest("12345")
	set actual = ##class(DB.Reports.MSGoPathology).FormatPracticeAddress(requestObj)
	set expected = "Test Practice Name"_$c(10)_"123 Main St"_$c(10)_"Boston MA 17654"
	do $$$AssertEquals(expected, actual, "Address v1.  Expected: "_ expected_" | Actual: "_actual)
}

Method TestGetAddress2()
{
	#dim requestObj AS DB.PathologyRequest
	set requestObj = ..GenerateRequest("12345",,,"Box 13")
	set actual = ##class(DB.Reports.MSGoPathology).FormatPracticeAddress(requestObj)
	set expected = "Test Practice Name"_$c(10)_"123 Main St Box 13"_$c(10)_"Boston MA 17654"
	do $$$AssertEquals(expected, actual, "Address with Line 2.  Expected: "_ expected_" | Actual: "_actual)
}

Method GenerateRequest(regNumber As %String, hcpProviderNumber As %String = "", practiceProviderNumber As %String = "", addressLine2 As %String = "") As DB.PathologyRequest
{
	#dim requestObj As DB.PathologyRequest
	set requestObj = ##class(Tests.Unit.DB.PathologyRequestStub).%New()
	set requestObj.Patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set requestObj.Patient.HealthCareProvider = ##class(DB.HealthCareProvider).%New()
	set requestObj.Patient.HealthCareProvider.HCPRegistrationNumber = regNumber
	set requestObj.Patient.HealthCareProvider.ProviderNumber = hcpProviderNumber
	set requestObj.Patient.Practice = ##class(DB.HealthCareProviderPractice).%New()
	set requestObj.Patient.Practice.ProviderNumber = practiceProviderNumber
	set requestObj.Patient.Practice.Practice = ..GeneratePractice(addressLine2)
	quit requestObj
}

Method GeneratePractice(line2 As %String = "") As DB.System.Practice
{
	set practiceObj = ##class(DB.System.Practice).%New()
	set practiceObj.Name = "Test Practice Name"
	set practiceObj.Address.AddressLine1 = "123 Main St"
	set practiceObj.Address.AddressLine2 = line2
	set practiceObj.Address.City = "Boston"
	set practiceObj.Address.State = "MA"
	set practiceObj.Address.Postcode = "17654"
	quit practiceObj
}

}
