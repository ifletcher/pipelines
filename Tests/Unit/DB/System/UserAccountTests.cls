Class Tests.Unit.DB.System.UserAccountTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Instance Methods ***/
Method TestLinkToPatientNullPatient()
{
    #dim userObj As DB.System.UserAccount
    set userObj = ..GetDisabledUserAccount()
    do ..AssertIsEmptyString(userObj.Patient, "Sanity Check: User Not yet linked.")
    set sc = userObj.LinkToPatient("")
    do $$$AssertNotTrue(sc, "Cannot link null string")
    do ..AssertIsEmptyString(userObj.Patient, "Patient should be empty")
    do ..AssertEquals(0, userObj.Project.Count(), "Project should be empty")
    do ..AssertEquals(1, userObj.AccountDisabled, "Account should remain disabled")
    do ..AssertEquals(1, userObj.FailedLoginCount, "Failed Login count should not be reset to 0")
}

Method TestLinkToPatientFailIsPatient()
{
    #dim userObj As DB.System.UserAccount
    set userObj = ..GetDisabledUserAccount("Admin")
    do ..AssertIsEmptyString(userObj.Patient, "Sanity Check: User Not yet linked.")
    set patientObj = ..GetPatient()
    set sc = userObj.LinkToPatient(patientObj)
    do $$$AssertNotTrue(sc, "Cannot link Patient to admin UserAccount record.")
    do ..AssertIsEmptyString(userObj.Patient, "Patient should be empty")
    do ..AssertEquals(0, userObj.Project.Count(), "Project should be empty")
    do ..AssertEquals(1, userObj.AccountDisabled, "Account should remain disabled")
    do ..AssertEquals(1, userObj.FailedLoginCount, "Failed Login count should not be reset to 0")
}

Method TestLinkToPatientFailAlredyLinked()
{
    #dim userObj As DB.System.UserAccount
    set userObj = ..GetDisabledUserAccount("Patient")
    set userObj.Patient = ..GetPatient("John", "Smith")
    set sc = userObj.Project.Insert(..GetProject("Sugar Pills"))
    do ..AssertEquals("John Smith", userObj.Patient.FullName, "Sanity Check: User already linked starts empty")

    set patientObj = ..GetPatient()
    set patientObj.Project = ..GetProject("Placebo")
    set sc = userObj.LinkToPatient(patientObj)

    do $$$AssertNotTrue(sc, "Cannot link Patient when user is already linked.")
    do ..AssertEquals("John Smith", userObj.Patient.FullName, "Patient should remain unchanged")
    do ..AssertEquals(1, userObj.Project.Count(), "Project should remain unchanged")
    do ..AssertEquals("Sugar Pills", userObj.Project.GetAt(1).Code, "Project should remain unchanged")
    do ..AssertEquals(1, userObj.AccountDisabled, "Account should remain disabled")
    do ..AssertEquals(1, userObj.FailedLoginCount, "Failed Login count should not be reset to 0")
}

Method TestLinkToPatientFailNoProject()
{
    #dim userObj As DB.System.UserAccount
    set userObj = ..GetDisabledUserAccount("Patient")
    do ..AssertIsEmptyString(userObj.Patient, "Sanity Check: User Not yet linked.")
    set patientObj = ..GetPatient()
    set sc = userObj.LinkToPatient(patientObj)
    do $$$AssertNotTrue(sc, "Cannot link Patient who doesn't have a project.")
    do ..AssertIsEmptyString(userObj.Patient, "Patient should be empty")
    do ..AssertEquals(0, userObj.Project.Count(), "Project should be empty")
    do ..AssertEquals(1, userObj.AccountDisabled, "Account should remain disabled")
    do ..AssertEquals(1, userObj.FailedLoginCount, "Failed Login count should not be reset to 0")
}

Method TestLinkToPatientSuccess()
{
    #dim userObj As DB.System.UserAccount
    set userObj = ..GetDisabledUserAccount("Patient")
    do ..AssertIsEmptyString(userObj.Patient, "Sanity Check: User Not yet linked.")
    set patientObj = ..GetPatient()
    set patientObj.Project = ..GetProject("Placebo")
    set sc = userObj.LinkToPatient(patientObj)
    do $$$AssertTrue(sc, "Successfuly link Patient to admin UserAccount record.")
    do ..AssertEquals("Bobby Tables", userObj.Patient.FullName, "Patient should be Linked")
    do ..AssertEquals("Placebo", userObj.Project.GetAt(1).Code, "Project should be Linked")
    do ..AssertEquals(0, userObj.AccountDisabled, "Account should be re-enabled")
    do ..AssertEquals(0, userObj.FailedLoginCount, "Failed Login count should be reset to 0")
}

/*** Helper Methods ***/
Method GetDisabledUserAccount(roleName = "") As DB.System.UserAccount
{
    set userObj = ##class(DB.System.UserAccount).%New()
    set userObj.Role = ..GetUserRole(roleName)
    set userObj.AccountDisabled = 1
    set userObj.FailedLoginCount = 1
    set userObj.Project = ""
    quit userObj
}

Method GetUserRole(name) As DB.System.UserRole
{
    quit:(name = "") ""
    set roleObj = ##class(DB.System.UserRole).%New()
    set roleObj.Name = name
    quit roleObj
}

Method GetPatient(fName = "Bobby", lName = "Tables") As Tests.Unit.DB.PatientStub
{
    set patObj = ##class(Tests.Unit.DB.PatientStub).%New()
    set patObj.FirstName = fName
    set patObj.LastName = lName
    quit patObj
}

Method GetProject(code = "") As DB.System.Project
{
    quit:(code = "") ""
    set project = ##class(DB.System.Project).%New()
    set project.Code = code
    quit project
}

}
