Class Tests.Unit.DB.System.UserRoleTests Extends Tests.AbstractTestCase
{

/*** Testing Instance Methods (should this be integrtion test?) ***/
Method TestIsPatient()
{
	set roleObj = ..GetUserRole("Patient")
    do $$$AssertTrue(roleObj.IsPatient(), "Role Is patient role.")
}

Method TestIsNotPatient()
{
	set roleObj = ..GetUserRole("Admin")
    do $$$AssertNotTrue(roleObj.IsPatient(), "Role is not patient role.")
}

Method GetUserRole(name) As DB.System.UserRole
{
    set roleObj = ##class(DB.System.UserRole).%New()
    set roleObj.Name = name
    quit roleObj
}

}
