Class Tests.Unit.DB.System.Utils.HTMLInjectionTests Extends Tests.AbstractTestCase
{

Method TestBasicXSS()
{
    set str = "<SCRIPT SRC=http://xss.rocks/xss.js></SCRIPT>"
    do ..AssertIsEmptyString(##class(DB.System.Utils).StripHTMLTags(str), "str should be empty after stripping HTML")
}

Method TestImageXSS()
{
    set str = "<IMG SRC="_"javascript:alert('XSS');"_">"
    do ..AssertIsEmptyString(##class(DB.System.Utils).StripHTMLTags(str), "str should be empty after stripping HTML")
}

Method TestRemoteStyleSheet()
{
    set str = "<LINK REL='stylesheet' HREF='http://xss.rocks/xss.css'>"
    do ..AssertIsEmptyString(##class(DB.System.Utils).StripHTMLTags(str), "str should be empty after stripping HTML")
}

Method TestBasicXSSMixedCase()
{
    set str = "<Script SRC=""http://xss.rocks/xss.js""></scrIPT>"
    do ..AssertIsEmptyString(##class(DB.System.Utils).StripHTMLTags(str), "str should be empty after stripping HTML")
}

Method TestImageXSSMixedCase()
{
    set str = "<Img SRC=""javascript:alert('XSS');"">"
    do ..AssertIsEmptyString(##class(DB.System.Utils).StripHTMLTags(str), "str should be empty after stripping HTML")
}

}
