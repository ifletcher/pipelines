Class Tests.Unit.DB.PatientDoseHistoryTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB:Tests.Unit.DB.PatientDoseHistoryTests", "/nodelete")

/*** Testing Instance Methods ***/
Method TestGetDaysOnPhaseNoPhase()
{
    #dim doseHistoryObj As DB.PatientDoseHistory
    set doseHistoryObj = ##class(DB.PatientDoseHistory).%New()

    do $$$AssertEquals("", doseHistoryObj.Phase, "Sanity Check: Phase on new object is null.")
    do $$$AssertEquals(0, doseHistoryObj.GetDaysOnPhase(), "Count should be zero if no phase present.")
}

}
