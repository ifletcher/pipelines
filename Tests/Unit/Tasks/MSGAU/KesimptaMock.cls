Class Tests.Unit.Tasks.MSGAU.KesimptaMock Extends Tests.Unit.Tasks.MSGAU.KesimptaStub
{

ClassMethod FindMostRecentShipment(patOBJ As DB.Patient)
{
    if (patOBJ.CurrentOrderItem = "") {
        return ""
    }

    return:(patOBJ.CurrentOrderItem.Description = "No Dispatch Date") 1
    return:(patOBJ.CurrentOrderItem.Description = "Recent Dispatch Date") 2
    return:(patOBJ.CurrentOrderItem.Description = "Old Dispatch Date") 3
    return ""
}

ClassMethod GetDispatchedTime(shipId)
{
    return:(shipId = 3) "2020-12-10 13:20:33"
    return:(shipId = 2) "2021-01-10 13:20:33"
    return ""
}

}
