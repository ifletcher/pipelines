Class Tests.Unit.DB.Reports.CSVReportTests Extends %UnitTest.TestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit.DB.Reports:Tests.Unit.DB.Reports.CSVReportTests", "/nodelete")

/*** Testing Class Methods ***/
Method TestCleanTextNullInput()
{
	set actual = ##class(DB.Reports.CSVReport).CleanText()
	set expected = ""
	do $$$AssertEquals(expected, actual, "No input should return empty string.")
}

Method TestCleanTextStripsControlCharacters()
{
    set data = "type"_$c(3)_"writer"
	set actual = ##class(DB.Reports.CSVReport).CleanText(data)
	set expected = "typewriter"
	do $$$AssertEquals(expected, actual, "Expected: "_expected_" Actual: "_actual)
}

Method TestCleanTextTranslatesCarriageReturns()
{
    set data = "type"_$c(13,10)_"writer"
	set actual = ##class(DB.Reports.CSVReport).CleanText(data)
	set expected = "type  writer"
	do $$$AssertEquals(expected, actual, "Expected: "_expected_" Actual: "_actual)
}

Method TestCleanTextEscapesQuotes()
{
    set data = "type,""writer"
	set actual = ##class(DB.Reports.CSVReport).CleanText(data)
	set expected = """type,""""writer"""
	do $$$AssertEquals(expected, actual, "Expected: "_expected_" Actual: "_actual)
}

Method TestFormatDateNull()
{
	set actual = ##class(DB.Reports.CSVReport).FormatDate()
	set expected = ""
	do $$$AssertEquals(expected, actual, "Expected: "_expected_" Actual: "_actual)
}

Method TestFormatDate1()
{
	set data = 65438  ; $zdh("02/29/2020")
	set actual = ##class(DB.Reports.CSVReport).FormatDate(data)
	set expected = "2020-02-29"
	do $$$AssertEquals(expected, actual, "Expected: "_expected_" Actual: "_actual)
}

Method TestGenerateCSVNullData()
{
	set outputFile = ##class(DB.Reports.CSVReport).GenerateCSV()
    do $$$AssertNotTrue(outputFile.%IsNull(), "Sanity check. A file should always be generated.")
    set actual = outputFile.ReadLine()
	set expected = "Column 1,Column 2,Etc"
	do $$$AssertEquals(expected, actual, "Expected header row: "_expected_" Actual: "_actual)
}

Method TestGenerateCSVNoData()
{
	set outputFile = ##class(DB.Reports.CSVReport).GenerateCSV(0)
    do $$$AssertNotTrue(outputFile.%IsNull(), "Sanity check. A file should always be generated.")
    set actual = outputFile.ReadLine()
	set expected = "Column 1,Column 2,Etc"
	do $$$AssertEquals(expected, actual, "Expected header row: "_expected_" Actual: "_actual)
}

Method TestGenerateCSVData()
{
    set dataRecords = ""
    set dataRecords($Increment(dataRecords)) = $listbuild("field 1", "field 2", "third field")
    set dataRecords($Increment(dataRecords)) = $listbuild("field"_$c(13,10)_" 1", "field"_$c(3)_"ish", "third, field")
    set dataRecords($Increment(dataRecords)) = $listbuild("", , "")
	
    set outputFile = ##class(DB.Reports.CSVReport).GenerateCSV(.dataRecords)
    do $$$AssertNotTrue(outputFile.%IsNull(), "Sanity check. A file should always be generated.")
    //
    set actual = outputFile.ReadLine()
	set expected = "Column 1,Column 2,Etc"
	do $$$AssertEquals(expected, actual, "Expected header row: "_expected_" Actual: "_actual)
    //
    set actual = outputFile.ReadLine()
	set expected = "field 1,field 2,third field"
	do $$$AssertEquals(expected, actual, "Expected row: "_expected_" Actual: "_actual)
    //
    set actual = outputFile.ReadLine()
	set expected = "field   1,fieldish,""third, field"""
	do $$$AssertEquals(expected, actual, "Expected row: "_expected_" Actual: "_actual)

	set actual = outputFile.ReadLine()
	set expected = ",,"
	do $$$AssertEquals(expected, actual, "Expected row: "_expected_" Actual: "_actual)
}

}
