Class Tests.Unit.DB.Reports.AdverseEvents.SDQCStub Extends DB.Reports.AdverseEvents.SDQC
{

ClassMethod GetAdverseEventData(ByRef records, startDate As %Date, endDate As %Date) As %Status
{
    kill records
    set records = 0
    quit $$$OK
}

ClassMethod GetPatientNoteHistoryData(ByRef records, startDateh As %Date, endDateh As %Date) As %Status
{
    kill records
    set records = 0
    quit $$$OK
}

ClassMethod GetHCPNoteHistoryData(ByRef records, startDate As %Date, endDate As %Date) As %Status
{
    kill records
    set records = 0
    quit $$$OK
}

ClassMethod Today()
{
    quit "65874,33010"  // May 10, 2021
}

}
