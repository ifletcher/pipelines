Class Tests.Unit.DB.System.DocumentsTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Instance Methods (should this be integrtion test?) ***/
Method TestGetDocumentNoCode()
{
	set sc = ""
	set documentBytes = ##class(DB.System.Documents).GetDocumentFromCodeProject("", "", .sc)
	do $$$AssertStatusOK(sc)
	do ..AssertIsEmptyString(documentBytes, "No document code should return empty string.")
}

Method TestGetDocumentNoRecord()
{
	set sc = ""
	set documentBytes = ##class(DB.System.Documents).GetDocumentFromCodeProject("Bogus_codez", "", .sc)
	do $$$AssertStatusOK(sc)
	do ..AssertIsEmptyString(documentBytes, "No project code or record should return empty string.")
}

Method TestGetEncodedDocumentFakeDocument()
{
	set doc = ##class(DB.System.Documents).%New()
	do doc.Document.Write("Don't cross the streams")
	set sc = ""
	set filebits = doc.GetEncodedDocument(.sc)
	do $$$AssertStatusOK(sc)
	do ..AssertEquals(filebits, "RG9uJ3QgY3Jvc3MgdGhlIHN0cmVhbXM=", "Document stream should return base 64 encoded value.")
}

}
