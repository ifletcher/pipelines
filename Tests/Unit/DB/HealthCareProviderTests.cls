Class Tests.Unit.DB.HealthCareProviderTests Extends Tests.AbstractTestCase
{

/*** Testing Class Methods ***/
Method TestGetIdByUserIdNullId()
{
    set actual = ##class(DB.HealthCareProvider).GetIdByUserId("")
	set expected = ""
	do ..AssertEquals(expected, actual, "No HCP Id should return empty string.")
}


}