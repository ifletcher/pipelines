Class Tests.Unit.DB.Reports.MSGoOCTTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

/*** Testing Class Methods ***/

/*
Method TestGetAddressNoPractice()
{
	set requestObj = ..GenerateRequest("12345")
	set requestObj.Patient.Practice = ""
	set actual = ##class(DB.Reports.MSGoOCT).FormatPracticeAddress(requestObj)
	set expected = ""
	do $$$AssertEquals(expected, actual, "No Patient Practice should return empty string")
}

Method TestGetAddress1()
{
	#dim requestObj AS DB.PathologyRequest
	set requestObj = ..GenerateRequest("12345")
	set actual = ##class(DB.Reports.MSGoOCT).FormatPracticeAddress(requestObj)
	set expected = "Test Practice Name"_$c(10)_"123 Main St"_$c(10)_"Boston MA 17654"
	do $$$AssertEquals(expected, actual, "Address v1.  Expected: "_ expected_" | Actual: "_actual)
}

Method TestGetAddress2()
{
	#dim requestObj AS DB.PathologyRequest
	set requestObj = ..GenerateRequest("12345",,,"Box 13")
	set actual = ##class(DB.Reports.MSGoOCT).FormatPracticeAddress(requestObj)
	set expected = "Test Practice Name"_$c(10)_"123 Main St Box 13"_$c(10)_"Boston MA 17654"
	do $$$AssertEquals(expected, actual, "Address with Line 2.  Expected: "_ expected_" | Actual: "_actual)
}
*/
Method TestGetCopyToField1NoPrimaryPractice()
{
	#dim requestObj AS DB.PathologyRequest = ..GenerateRequest("hcp@example.com")
	set requestObj.Patient.Practice = ""
	set actual = ##class(DB.Reports.MSGoOCT).GetCopyToField1(requestObj)
	set expected = "hcp@example.com"
	do ..AssertEquals(expected, actual, "When there is no Primary Practice, Copy To should be HCP's email.")
}

Method TestGetCopyToField1NoPrimaryPracticeEmail()
{
	#dim requestObj AS DB.PathologyRequest = ..GenerateRequest("hcp@example.com")
	set actual = ##class(DB.Reports.MSGoOCT).GetCopyToField1(requestObj)
	set expected = "hcp@example.com"
	do ..AssertEquals(expected, actual, "When there is no Primary Practice Email, fall back to HCP's email.")
}

Method TestGetCopyToField1PrimaryPracticeEmail()
{
	#dim requestObj AS DB.PathologyRequest = ..GenerateRequest("hcp@example.com", "patient.practice@example.com")
	set actual = ##class(DB.Reports.MSGoOCT).GetCopyToField1(requestObj)
	set expected = "patient.practice@example.com"
	do ..AssertEquals(expected, actual, "When there is a Primary Practice email, Copy To should be that Practice Email email.")
}

Method GenerateRequest(hcpEmail = "", ptPrimaryPracticeEmail = "") As DB.PathologyRequest
{
	#dim requestObj As DB.PathologyRequest
	set requestObj = ##class(Tests.Unit.DB.PathologyRequestStub).%New()
	set requestObj.Patient = ##class(Tests.Unit.DB.PatientStub).%New()
	set requestObj.Patient.HealthCareProvider = ##class(DB.HealthCareProvider).%New()
	set requestObj.Patient.HealthCareProvider.EmailAddress = hcpEmail
	set requestObj.Patient.Practice = ##class(DB.HealthCareProviderPractice).%New()
	set requestObj.Patient.Practice.Practice = ..GeneratePractice("Patient Practice", ptPrimaryPracticeEmail)
	quit requestObj
}

Method GeneratePractice(name As %String = "Test Practice", email As %String = "") As DB.System.Practice
{
	set practiceObj = ##class(DB.System.Practice).%New()
	set practiceObj.Name = name
	set practiceObj.EmailAddress = email
	/*
	set practiceObj.Address.AddressLine1 = "123 Main St"
	set practiceObj.Address.AddressLine2 = line2
	set practiceObj.Address.City = "Boston"
	set practiceObj.Address.State = "MA"
	set practiceObj.Address.Postcode = "17654"
	*/
	quit practiceObj
}

}
