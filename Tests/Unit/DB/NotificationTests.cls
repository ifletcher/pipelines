Class Tests.Unit.DB.NotificationTests Extends Tests.AbstractTestCase
{

// d ##class(%UnitTest.Manager).RunTest("Unit", "/nodelete")

Method TestActionNotificationNoId()
{
	set expected = 0
	set actual = ##class(DB.Notification).ActionNotification("")
	do $$$AssertStatusEquals(actual, expected)
}

Method TestActionNotificationBogusId()
{
	set expected = 0
	set actual = ##class(DB.Notification).ActionNotification("Bogus Id")
	do $$$AssertStatusEquals(actual, expected)
}

}
