Class Tests.Unit.DB.Reports.Shipments.ReorderDrugSpy Extends DB.Reports.Shipments.ReorderDrug
{

Property ObservedStartDate As %String;

Property ObservedEndDate As %String;

Property ReferenceDate As %String;

Method %Today()
{
	quit ..ReferenceDate
}

Method GetData(ByRef records, startDate As %Date, endDate As %Date, pTask As %Boolean = 1, tracking = "") As %Status
{
    kill records
    kill:(tracking'="") @tracking

	set ..ObservedStartDate = startDate
	set ..ObservedEndDate = endDate

	quit $$$OK
}

}
